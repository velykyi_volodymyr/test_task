package com.velykyi.controller;

import com.velykyi.model.Department;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


import java.util.List;

public class CRUDDepartment {
    private static SessionFactory factory = InputProcessor.getFactory();

    public void add(String name, String head) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Department department = new Department(name, head);
            session.save(department);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Department getByName(String departmentName) {
        Session session = factory.openSession();
        String hql = "SELECT D FROM Department D WHERE D.name = '" + departmentName + "'";
        Query departmentQuery = session.createQuery(hql);
        return (Department) departmentQuery.list().get(0);
    }

    public Department getById(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        Department department = null;
        try {
            transaction = session.beginTransaction();
            department = (Department) session.get(Department.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return department;
    }

    public List getAll() {
        Session session = factory.openSession();
        Transaction transaction = null;
        List departments = null;
        try {
            transaction = session.beginTransaction();
            departments = session.createQuery("FROM Department").list();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return departments;
    }

    public void update(int id, String head) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Department department = (Department) session.get(Department.class, id);
            department.setHead(head);
            session.update(department);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void delete(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Department department = (Department) session.get(Department.class, id);
            session.delete(department);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
