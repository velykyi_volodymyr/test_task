package com.velykyi.controller;

import com.velykyi.model.Department;
import com.velykyi.model.Lector;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


import java.util.List;

public class CRUDLector {
    private static SessionFactory factory = InputProcessor.getFactory();

    public void add(int lectorId, String firstName, String lastName, String degree, int department, int salary) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Lector lector = new Lector(lectorId, firstName, lastName, degree, department, salary);
            session.save(lector);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Lector getByLastName(Department department) {
        Session session = factory.openSession();
        String hql = "SELECT L FROM Lector L WHERE L.lastName = '" +
                department.getHead()+ "'";
        return (Lector)session.createQuery(hql).list().get(0);
    }

    public Lector getById(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        Lector lector = null;
        try {
            transaction = session.beginTransaction();
            lector = (Lector) session.get(Lector.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return lector;
    }

    public List<Lector> searchInName(String condition) {
        Session session = factory.openSession();
        String hql = "SELECT L FROM Lector L WHERE (L.firstName LIKE '%"
                + condition + "%' OR L.lastName LIKE '%" + condition + "%')";
        return session.createQuery(hql, Lector.class).list();
    }

    public Long getCount(Department department) {
        Session session = factory.openSession();
        String hql = "SELECT count(L) FROM Lector L WHERE L.department = " + department.getId();
        return (Long)session.createQuery(hql).getSingleResult();
    }

    public Object getCountByDegree(Department department, String degree) {
        Session session = factory.openSession();
        String hql = "SELECT count(L) FROM Lector L WHERE (L.department = " + department.getId()
                + " AND L.degree = " + degree + ")";
        return session.createQuery(hql).getSingleResult();
    }

    public Long getSumBySalary(Department department) {
        Session session = factory.openSession();
        String hql = "SELECT sum(L.salary) FROM Lector L WHERE L.department = " + department.getId();
        return (Long)session.createQuery(hql).getSingleResult();
    }

    public List getAll() {
        Session session = factory.openSession();
        Transaction transaction = null;
        List lectors = null;
        try {
            transaction = session.beginTransaction();
            lectors = session.createQuery("FROM Lector").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return lectors;
    }

    public void update(int id, String degree) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Lector lector = (Lector) session.get(Lector.class, id);
            lector.setDegree(degree);
            session.update(lector);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void delete(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Lector lector = (Lector) session.get(Lector.class, id);
            session.delete(lector);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
