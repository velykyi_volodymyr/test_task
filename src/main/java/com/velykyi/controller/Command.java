package com.velykyi.controller;

import com.velykyi.model.Department;
import com.velykyi.model.Lector;

import java.util.List;

public class Command {
    private static CRUDDepartment crudDepartment = new CRUDDepartment();
    private static CRUDLector crudLector = new CRUDLector();

    public List search(String condition) {
        return crudLector.searchInName(condition);
    }

    public Long getCount(String condition) {
        Department department = crudDepartment.getByName(condition);
        return crudLector.getCount(department);
    }

    public double getAvrSalary(String condition) {
        Department department = crudDepartment.getByName(condition);
        Long count = crudLector.getCount(department);
        Long salarySum = crudLector.getSumBySalary(department);
        return salarySum/count;
    }

    public Long[] getStatistic(String condition) {
        Long[] statistic = new Long[3];
        Department department = crudDepartment.getByName(condition);
        statistic[0] = (Long)crudLector.getCountByDegree(department, "assistant");
        statistic[1] = (Long)crudLector.getCountByDegree(department, "associate professor");
        statistic[2] = (Long)crudLector.getCountByDegree(department, "professor");
        return statistic;
    }

    public Lector getHead(String condition) {
        Department department = crudDepartment.getByName(condition);
        return crudLector.getByLastName(department);
    }
}
