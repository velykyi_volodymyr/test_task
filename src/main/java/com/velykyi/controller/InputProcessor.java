package com.velykyi.controller;

import com.velykyi.model.Department;
import com.velykyi.model.Lector;
import com.velykyi.view.View;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.*;

public class InputProcessor {
    private static Map<String, Printable> methodMenu;
    private static View view;
    private String commandCondition;
    private Command command;
    private static SessionFactory factory;

    static{
        try {
            factory = new Configuration().configure().
                    addAnnotatedClass(Department.class).addAnnotatedClass(Lector.class).
                    buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public InputProcessor() {
        command = new Command();
        view = new View();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::printHead);
        methodMenu.put("2", this::printStatistic);
        methodMenu.put("3", this::printAvrSalary);
        methodMenu.put("4", this::printCount);
        methodMenu.put("5", this::printSearchResult);
    }

    public static void main(String[] args) {
        InputProcessor inputProcessor = new InputProcessor();
        String[] keyMenu;
        do {
            keyMenu = view.printMenu();
            try {
                inputProcessor.commandCondition = keyMenu[1];
                methodMenu.get(keyMenu[0]).print();
            } catch (Exception e) {

            }
        } while (!keyMenu[0].equals("0"));
        System.exit(1);
    }

    public static SessionFactory getFactory() {
        return factory;
    }

    private void printSearchResult() {
        view.printSearchResult(command.search(commandCondition));
    }

    private void printCount() {
        view.printCount(command.getCount(commandCondition));
    }

    private void printAvrSalary() {
        view.printAvrSalary(commandCondition, command.getAvrSalary(commandCondition));
    }

    private void printStatistic() {
        view.printStatistic(command.getStatistic(commandCondition));
    }

    private void printHead() {
        Lector lector = command.getHead(commandCondition);
        view.printHead(commandCondition, lector.getFirstName() + " " + lector.getLastName() );
    }
}
