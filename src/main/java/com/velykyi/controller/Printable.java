package com.velykyi.controller;

@FunctionalInterface
public interface Printable {
    void print();
}
