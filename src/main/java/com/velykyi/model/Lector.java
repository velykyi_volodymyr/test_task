package com.velykyi.model;

import javax.persistence.*;

@Entity
@Table(name = "LECTOR")
public class Lector {
    @Id@GeneratedValue
    @Column(name = "id")
    private int id;
    @Column(name = "lector_id")
    private int lectorId;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "degree")
    private String degree;
    @Column(name = "department")
    private int department;
    @Column(name = "salary")
    private int salary;

    public Lector() {
    }

    public Lector(int lectorId, String firstName, String lastName, String degree, int department, int salary) {
        this.lectorId = lectorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.degree = degree;
        this.department = department;
        this.salary = salary;
    }

    public int getLectorId() {
        return lectorId;
    }

    public void setLectorId(int lectorId) {
        this.lectorId = lectorId;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
