package com.velykyi.view;

import com.velykyi.model.Department;
import com.velykyi.model.Lector;

import java.util.*;

public class View {
    private static Map<String, String> menu;
    private static Scanner scann = new Scanner(System.in);

    static {
        menu = new LinkedHashMap<>();
        menu.put("1", "Who is head of department ");
        menu.put("2", "Show statistic of department ");
        menu.put("3", "Show the average salary for department ");
        menu.put("4", "Show count of employee for ");
        menu.put("5", "Global search by ");
        menu.put("q", "Exit");
    }

    public String[] printMenu() {
        System.out.println("\nYou can type next commands:\n");
        for (String str : menu.values()) {
            System.out.println(str);
        }
        System.out.println("\nPlease, enter your command.");
        return this.checkCommand(scann.nextLine());
    }

    public void printSearchResult(List<Lector> result) {
        System.out.print("\nAnswer: ");
        for (Lector item : result) {
            System.out.println(item);
        }
    }

    public void printCount(Long count) {
        System.out.println("Answer: Count of employee: " + count);
    }

    public void printAvrSalary(String department, double avrSalary) {
        System.out.println("Answer: The  average salary of " + department
                + " department is " + avrSalary);
    }

    public void printStatistic(Object[] statistic) {
        System.out.println("Answer: assistant - " + statistic[0]);
        System.out.println("associate professor - " + statistic[1]);
        System.out.println("professor - " + statistic[2]);
    }

    public void printHead(String department, String head) {
        System.out.println("Answer: Head of " + department + " department is "
                + head);
    }

    private String[] checkCommand(String command) {
        String[] optionalCommand = new String[2];
        if (command.toLowerCase().contains("who is head of department ")) {
            optionalCommand[0] = "1";
            optionalCommand[1] = command.toLowerCase().replace("who is head of department ", "").toLowerCase();
        } else if (command.toLowerCase().contains("show statistic for ")) {
            optionalCommand[0] = "2";
            optionalCommand[1] = command.toLowerCase().replace("show statistic for ", "").toLowerCase();
        } else if (command.toLowerCase().contains("show the average salary for department ")) {
            optionalCommand[0] = "3";
            optionalCommand[1] = command.toLowerCase().replace("show the average salary for department ", "").toLowerCase();
        } else if (command.toLowerCase().contains("show count of employee for ")) {
            optionalCommand[0] = "4";
            optionalCommand[1] = command.toLowerCase().replace("show count of employee for ", "").toLowerCase();
        } else if (command.toLowerCase().contains("global search by ")) {
            optionalCommand[0] = "5";
            optionalCommand[1] = command.toLowerCase().replace("global search by ", "").toLowerCase();
        } else if (command.toLowerCase().contains("exit")){
            optionalCommand[0] = "0";
        } else {
            optionalCommand[0] = "-1";
        }
        return optionalCommand;
    }
}
